from random import randint

user_name = input("What's your name? ")

# guess 5 times
# can also use guess_number (range5)
# guess 1
for guess_number in range(1, 6):
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)

    print("Okay let me guess :", user_name, "were you born in",
          month_number, "/", year_number, "?")

    response = input("yes or no? ")

    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I have better things to do, goodbye")
    else:
        print("no way! lemme try again!")
